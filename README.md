# ORCropImageController

[![CI Status](http://img.shields.io/travis/Nikita Egoshin/ORCropImageController.svg?style=flat)](https://travis-ci.org/Nikita Egoshin/ORCropImageController)
[![Version](https://img.shields.io/cocoapods/v/ORCropImageController.svg?style=flat)](http://cocoapods.org/pods/ORCropImageController)
[![License](https://img.shields.io/cocoapods/l/ORCropImageController.svg?style=flat)](http://cocoapods.org/pods/ORCropImageController)
[![Platform](https://img.shields.io/cocoapods/p/ORCropImageController.svg?style=flat)](http://cocoapods.org/pods/ORCropImageController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ORCropImageController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ORCropImageController"
```

## Author

Nikita Egoshin, nikita.egoshin@omega-r.com

## License

ORCropImageController is available under the MIT license. See the LICENSE file for more info.
